//Gets list of kinects that are available and puts them in a drop down selector
var xhttpKinectsIO = new XMLHttpRequest();
xhttpKinectsIO.open("POST", "/get/Kinects/info", true);
xhttpKinectsIO.send();

xhttpKinectsIO.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var kinectIOList = JSON.parse(xhttpKinectsIO.response);
        var table = document.getElementById("in-out-table");

        for (var i = 0; i < kinectIOList.length; i++){
            var tr = document.createElement('TR');

            var td1 = document.createElement('TD');
            td1.appendChild(document.createTextNode(kinectIOList[i].name));
            var td2 = document.createElement('TD');
            td2.appendChild(document.createTextNode(kinectIOList[i].roomIn));
            var td3 = document.createElement('TD');
            td3.appendChild(document.createTextNode(kinectIOList[i].roomOut));

            tr.appendChild(td1);
            tr.appendChild(td2);
            tr.appendChild(td3);

            table.appendChild(tr);
        }
    }
};