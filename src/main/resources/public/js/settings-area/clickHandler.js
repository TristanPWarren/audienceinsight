// Handles adding a new room to the database.
function addRoomToDB() {
    var xhttpRooms = new XMLHttpRequest();

    var input = document.getElementById("new-room-name");
    var inputData = encodeURIComponent(input.value.replace(/\s/g,''));

    if (inputData === "") {
        alert("Room name cannot be null.");
        return;
    }

    xhttpRooms.open("POST", "/add/Room", true);
    xhttpRooms.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhttpRooms.send("roomName="+inputData);
    xhttpRooms.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 201) {
            alert("Room successfully added.");
            location.reload();
            return;
        }
        if (this.readyState === 4 && this.status === 500) {
            alert("Error 500: Problem adding room. Note: Name must be unique.");
            location.reload();
            return;
        }
    };
};

// Handles modifying the room name.
function modRmName() {
    var xhttpRooms = new XMLHttpRequest();

    var oldName = document.getElementById("mrn-old-name");
    var oldNameData = encodeURIComponent(oldName.options[oldName.selectedIndex].value);
    var newName = document.getElementById("mrn-new-name");
    var newNameData = encodeURIComponent(newName.value.replace(/\s/g,''));

    if (oldNameData === "--Select%20Room--" || newNameData === "") {
        alert("Field left blank. Both fields must have data in them.");
        return;
    }

    xhttpRooms.open("POST", "/update/Room", true);
    xhttpRooms.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhttpRooms.send("oldName="+oldNameData+"&newName="+newNameData);
    xhttpRooms.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            alert("Room name successfully modified.");
            location.reload();
            return;
        }
        if (this.readyState === 4 && this.status === 500) {
            alert("Error 500: Problem modifying room name. Note: Name must be unique.");
            location.reload();
            return;
        }
    };
};

// Handles modifying the kinect name.
function modKntName() {
    var xhttpKinects = new XMLHttpRequest();

    var oldName = document.getElementById("mkn-old-name");
    var oldNameData = encodeURIComponent(oldName.options[oldName.selectedIndex].value);
    var newName = document.getElementById("mkn-new-name");
    var newNameData = encodeURIComponent(newName.value.replace(/\s/g,''));

    if (oldNameData === "--Select%20Kinect--" || newNameData === "") {
        alert("Field left blank. Both fields must have data in them.");
        return;
    }

    xhttpKinects.open("POST", "/update/Kinect", true);
    xhttpKinects.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhttpKinects.send("oldName="+oldNameData+"&newName="+newNameData);
    xhttpKinects.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 200) {
            alert("Kinect name successfully modified.");
            location.reload();
            return;
        }
        if (this.readyState === 4 && this.status === 500) {
            alert("Error 500: Problem modifying kinect name. Note: Name must be unique.");
            location.reload();
            return;
        }
    };
};

// Handles adding room id and kinect id entry to room in table.
function addRoomIn(){
    var xhttpRKIO = new XMLHttpRequest();

    var kNam = document.getElementById("ari-kd");
    var kNamData = encodeURIComponent(kNam.options[kNam.selectedIndex].value);
    var rNam = document.getElementById("ari-rd");
    var rNamData = encodeURIComponent(rNam.options[rNam.selectedIndex].value);

    if (kNamData === "--Select%20Kinect--" || rNamData === "--Select%20Room--") {
        alert("Field left blank. Both fields must have data in them.");
        return;
    }

    xhttpRKIO.open("POST", "/add/Kinect/in", true);
    xhttpRKIO.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhttpRKIO.send("kinect="+kNamData+"&roomIn="+rNamData);
    xhttpRKIO.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 201) {
            alert("Successfully linked kinect and room.");
            location.reload();
            return;
        }
        if (this.readyState === 4 && this.status === 500) {
            alert("Error 500: Problem linking kinect and room. Note: Only one Room In per kinect.");
            location.reload();
            return;
        }
    };
}

// Handles adding room id and kinect id entry to room out table.
function addRoomOut(){
    var xhttpRKIO = new XMLHttpRequest();

    var kNam = document.getElementById("aro-kd");
    var kNamData = encodeURIComponent(kNam.options[kNam.selectedIndex].value);
    var rNam = document.getElementById("aro-rd");
    var rNamData = encodeURIComponent(rNam.options[rNam.selectedIndex].value);

    if (kNamData === "--Select%20Kinect--" || rNamData === "--Select%20Room--") {
        alert("Field left blank. Both fields must have data in them.");
        return;
    }

    xhttpRKIO.open("POST", "/add/Kinect/out", true);
    xhttpRKIO.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhttpRKIO.send("kinect="+kNamData+"&roomOut="+rNamData);
    xhttpRKIO.onreadystatechange = function() {
        if (this.readyState === 4 && this.status === 201) {
            alert("Successfully linked kinect and room.");
            location.reload();
            return;
        }
        if (this.readyState === 4 && this.status === 500) {
            alert("Error 500: Problem linking kinect and room. Note: Only one Room Out per kinect.");
            location.reload();
            return;
        }
    };
}