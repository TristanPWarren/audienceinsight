// LIST OF ROOMS SECTION
var listOfRooms = [];
var listOfRemoved = [];

var colourNumber = 0;

//Class that defines a room
class Room {
    constructor(objectID, roomID, date, child, adult, range, time, colour) {
        this._objectID = objectID;
        this._roomID = roomID;
        this._date = date;
        this._child = child;
        this._adult = adult;
        this._range = range;
        this._time = time;
        this._colour = colour;
    }

    get objectID() {
        return this._objectID;
    }

    get roomID() {
        return this._roomID;
    }

    get date() {
        return this._date;
    }

    get child() {
        return this._child;
    }

    get adult() {
        return this._adult;
    }

    get range() {
        return this._range;
    }

    get time() {
        return this._time;
    }

    get colour() {
        return this._colour;
    }

    //TODO Make this better
    set range(newRange) {
        if ((newRange === 0) || (newRange === 1) || (newRange === 2) || (newRange === 3) || (newRange === 4)) {
            this._age = newRange;
        } else {
            console.log("Invalid range");
        }
    }

    //TODO add validation
    set date(newDate) {
        this._date = newDate;
    }

    //TODO add validation
    set time(newTime) {
        this._time = newTime;
    }
};

//Gets list of rooms that are available and puts them in a drop down selector
var xhttpRooms = new XMLHttpRequest();
xhttpRooms.open("POST", "/get/Rooms", true);

xhttpRooms.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var roomList = JSON.parse(xhttpRooms.response);
//        console.log(roomList);
//        console.log(roomList[0]);
        for (i = 0; i < roomList.length; i++) {
            var option = document.createElement("option");
            var textOption = document.createTextNode(roomList[i]);
            option.appendChild(textOption);
            document.getElementById("room-dropper").appendChild(option);
        }
    }

};

function sliderUpdate() {
    var sliderValue = document.getElementById("room-slider").value;
    updateScale(sliderValue);
}

xhttpRooms.send();

// Draws the rooms onto the page (Once a room object has been created it is added to a list on the page)
function addRoomToPage(room) {
    var listContainer = document.createElement("div");
    listContainer.setAttribute("id", `list-container${room._objectID}`);
    listContainer.setAttribute("class", "list-container");

    var roomNameTitle = document.createElement("div");
    roomNameTitle.setAttribute("class", "room-name-title lor-list");
        var listName = document.createElement("div");
        listName.setAttribute("class", "list-text-lor");
            var text = document.createTextNode(room._roomID);
            listName.appendChild(text);
        roomNameTitle.appendChild(listName);

    var dateTitle = document.createElement("div");
    dateTitle.setAttribute("class", "date-title lor-list");
        var listDate = document.createElement("div");
        listDate.setAttribute("class", "list-text-lor");
        listDate.setAttribute("id", `list-date-text${room._objectID}`);
//            var text = document.createTextNode(makeDateToString(room._date));
            var text = document.createTextNode(room._date);
            listDate.appendChild(text);
        dateTitle.appendChild(listDate);

    var childTitle = document.createElement("div");
    childTitle.setAttribute("class", "child-title lor-list");
        var listChild = document.createElement("div");
        listChild.setAttribute("class", "list-text-lor");
            var text = document.createTextNode(room.child);
            listChild.appendChild(text);
        childTitle.appendChild(listChild);

    var adultTitle = document.createElement("div");
    adultTitle.setAttribute("class", "adult-title lor-list");
        var listAdult = document.createElement("div");
        listAdult.setAttribute("class", "list-text-lor");
            var text = document.createTextNode(room.adult);
            listAdult.appendChild(text);
        adultTitle.appendChild(listAdult);

    console.log("Should add button");
    var removeButton = document.createElement("div");
    removeButton.setAttribute("class", "remove-button lor-list");
    removeButton.setAttribute("onClick", `removeRoom(${room._objectID})`);
        var removeButtonText = document.createElement("div");
        removeButtonText.setAttribute("class", "list-text-lor remove-text");
            var text = document.createTextNode("Remove");
            removeButtonText.appendChild(text);
        removeButton.appendChild(removeButtonText);

    listContainer.appendChild(roomNameTitle);
    listContainer.appendChild(dateTitle);
    listContainer.appendChild(childTitle);
    listContainer.appendChild(adultTitle);
    listContainer.appendChild(removeButton);

    document.getElementById("lor-rooms").appendChild(listContainer);

};

// Creates a new room object and adds it to the list
function addNewRoom() {
    // Get values from form
    name = document.getElementById("room-dropper").value;
    date = document.getElementById("date-picker").value;
    child = document.getElementById("child-box").checked;
    adult = document.getElementById("adult-box").checked;
    range = rangeState;
    time = 12;


    if (name === "--Select Room--") {
        alert("Please select a room from the list");
    } else if (date === "") {
        alert("Please select a date");
    } else if ((child === false) && (adult === false)) {
        alert("Please select either child or adult");
    } else {
        // Reset form values
        document.getElementById("room-dropper").value = "--Select Room--";
        document.getElementById("date-picker").value = "";
        document.getElementById("child-box").checked = true;
        document.getElementById("adult-box").checked = true;

        standardFormatDate = new Date(date);

        colourNumber = (colourNumber + 1) % 6;

        temp = new Room(listOfRooms.length, name, standardFormatDate, child, adult, range, time, colourNumber);
        listOfRooms.push(temp);
        addRoomToPage(temp);

        addRoomToGraph(temp);
    }

};

function removeRoom(roomID) {
    listOfRemoved.push(roomID);

    document.getElementById("lor-rooms").removeChild(document.getElementById(`list-container${roomID}`));
    config.data.datasets[(2 * roomID) + 1].data = [];
    config.data.datasets[(2 * roomID) + 1].label = "remove";
    config.data.datasets[(2 * roomID)].data = [];
    config.data.datasets[(2 * roomID)].label = "remove";
    window.myLine.update();

    console.log(listOfRooms);
}

