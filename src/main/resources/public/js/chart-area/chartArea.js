//VERY important, do not delete
var rangeState = 2;

var checkForOverlap  = 0; //When the chart is updated quickly data is received repeatedly this change the state is the same as that which requested the data

var colours = ["rgba(255, 115, 130, 1)", "rgba(255, 162, 62, 1)", "rgba(68, 190, 194, 1)", "rgba(24, 166, 235, 1)", "rgba(148, 123, 253, 1)", "rgba(255, 204, 88, 1)"];

var dataInput = [];
var config = {
    type: 'line',
    data: {
        labels: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
        datasets: []
    },
    options: {
        responsive: true,
        legend: {
            labels: {
                filter: function(legendItem, chartData) {           //Removes duplicate label and data that is no longer needed
                    if (legendItem.datasetIndex % 2 === 0) {
                        return false;
                    } else if (legendItem.text === "remove") {
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        },
        title:{
            display:true,
            text:''
        },
        tooltips: {
            mode: 'index',
            intersect: false,
            filter: function(legendItem, chartData) {           //Removes duplicate label
                if (legendItem.datasetIndex % 2 === 1) {
                    return false;
                } else {
                    return true;
                }
            }
        },
        hover: {
            mode: 'nearest',
            intersect: true
        },
        scales: {
            xAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: ''
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Total Number Of Visitors'
                },
                ticks: {
                    max: 1
                }
            }]
        }
    }
};

window.onload = function() {
    var ctx = document.getElementById("main-chart").getContext("2d");
    window.myLine = new Chart(ctx, config);
};

//Formats date into form that can be sent to server
function makeDateToString(tempDate) {
    var newDate = (tempDate.getFullYear().toString());
    newDate = newDate.concat("-");
    if (parseInt(tempDate.getMonth() + 1) < 10) {
        newDate = newDate.concat("0");
    }
    newDate = newDate.concat((tempDate.getMonth() + 1).toString());
    newDate = newDate.concat("-");
    if (parseInt(tempDate.getDate()) < 10) {
        newDate = newDate.concat("0");
    }
    newDate = newDate.concat(tempDate.getDate().toString());

    return (newDate);
}

//When the date range is changed update the date in LOR
function updateLORDates() {
    for (i = 0; i < listOfRooms.length; i++ ) {
        if (listOfRemoved.indexOf(i) == -1 ){           //Checks element hasn't been removed
            document.getElementById(`list-date-text${i}`).innerHTML = listOfRooms[i]._date;
        }
    }
}

//Changes the date/time of all the room objects and then updates the graph
function changeRangeTime(direction) {

    //Updates rooms time attribute by an hour
    if (rangeState === 0) {
        for (i = 0; i < listOfRooms.length; i++) {
            if (direction === 0) {
                //Moves day back if at midnight
                if (listOfRooms[i]._time === 0) {
                    listOfRooms[i]._date.setDate(listOfRooms[i]._date.getDate() - 1);
                }
                listOfRooms[i]._time = (((listOfRooms[i]._time - 1) +24) % 24);
            } else {
                //Moves day forward if at midnight
                if (listOfRooms[i]._time === 23) {
                    listOfRooms[i]._date.setDate(listOfRooms[i]._date.getDate() + 1);
                }
                listOfRooms[i]._time = (((listOfRooms[i]._time + 1) + 24) % 24);
            }
        }
        updateLORDates();
        updateScale(0);
    }

        //Updates rooms date attribute by a day
    else if (rangeState === 1) {
        for (i = 0; i < listOfRooms.length; i++) {
            if (direction === 0) {
                listOfRooms[i]._date.setDate(listOfRooms[i]._date.getDate() - 1);
            } else {
                listOfRooms[i]._date.setDate(listOfRooms[i]._date.getDate() + 1);
            }
        }
        updateLORDates();
        updateScale(1);
    }

        //Updates rooms date attribute by a week
    else if (rangeState === 2) {
        for (i = 0; i < listOfRooms.length; i++) {
            if (direction === 0) {
                listOfRooms[i]._date.setDate(listOfRooms[i]._date.getDate() - 7);
            } else {
                listOfRooms[i]._date.setDate(listOfRooms[i]._date.getDate() + 7);
            }
        }
        updateLORDates();
        updateScale(2);
    }

        //Updates rooms date attribute by a month  WARNING: getMonth() and setMonth() start at 0
    else if (rangeState === 3) {
        for (i = 0; i < listOfRooms.length; i++) {
            if (direction === 0) {
                listOfRooms[i]._date.setMonth(listOfRooms[i]._date.getMonth() - 1);
            } else {
                listOfRooms[i]._date.setMonth(listOfRooms[i]._date.getMonth() + 1);
            }
        }
        updateLORDates();
        updateScale(3);
    }

        //Updates rooms date attribute by a year
    else if (rangeState === 4) {
        for (i = 0; i < listOfRooms.length; i++) {
            if (direction === 0) {
                listOfRooms[i]._date.setYear(listOfRooms[i]._date.getFullYear() - 1);
            } else {
                listOfRooms[i]._date.setYear(listOfRooms[i]._date.getFullYear() + 1);
            }
        }
        updateLORDates();
        updateScale(4);
    }
};

//Left button does this
function helperLeft() {
    changeRangeTime(0);
};

//Right button does this
function helperRight() {
    changeRangeTime(1);
};

//When a date range is changed the graph needs to be updated
function updateScale(scale) {
    checkForOverlap ++;
    config.data.datasets = [];
    config.options.scales.yAxes[0].ticks.max = 1; //Changes range to the smallest possible value

    switch(parseInt(scale)) {
        case 0:
            // Hourly
            rangeState = 0;
            if (listOfRooms.length > 0) {
                time = listOfRooms[0]._time;
            } else {
                time = 12;
            }
            config.options.title.text = "Hourly";
            config.data.labels = [(time.toString() + ":00"), (time.toString() + ":10"), (time.toString() + ":20"), (time.toString() + ":30"), (time.toString() + ":40"), (time.toString() + ":50"), ((time + 1).toString() + ":00")];
            for (i = 0; i < listOfRooms.length; i++) {
                listOfRooms[i]._range = 0;
            }
            break;

        case 1:
            // Daily
            rangeState = 1;
            config.options.title.text = "Daily";
            config.data.labels = ["0am", ,"2am", ,"4am", ,"6am", ,"8am", ,"10am", ,"12pm", ,"2pm", ,"4pm", ,"6pm", ,"8pm", ,"10pm", ,"0am"];
            for (i = 0; i < listOfRooms.length; i++) {
                listOfRooms[i]._range = 1;
            }
            break;

        case 2:
            // Weekly
            rangeState = 2;
            config.options.title.text = "Weekly";
            config.data.labels = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
            for (i = 0; i < listOfRooms.length; i++) {
                listOfRooms[i]._range = 2;
            }
            break;

        case 3:
            // Monthly
            rangeState = 3;
            config.options.title.text = "Monthly";
            config.data.labels = ["1st", , , ,"5th", , , , ,"10th", , , , ,"15th", , , , ,"20th", , , , , "25th", , , , ,"30th", ];
            for (i = 0; i < listOfRooms.length; i++) {
                listOfRooms[i]._range = 3;
            }
            break;

        case 4:
            //Yearly
            rangeState = 4;
            config.options.title.text = "Yearly";
            config.data.labels = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
            for (i = 0; i < listOfRooms.length; i++) {
                listOfRooms[i]._range = 4;
            }
            break;

        default:
    }

    //Updates labels even when there is no data
    window.myLine.update();

    //Adds rooms to graph
    for (i = 0; i < listOfRooms.length; i++) {
        addRoomToGraph(listOfRooms[i]);
    }
};

//Makes room object that can be passed by the server
function makePOSTData(roomObject) {
    var postString = "";
    postString = postString.concat("roomName=");
    postString = postString.concat(roomObject._roomID);
    postString = postString.concat("&startDate=");

    //Makes date the beginning of the week/month etc..
    tempDate = new Date(roomObject._date);
    if (rangeState === 2) {
        tempDate.setDate(roomObject._date.getDate() - roomObject._date.getDay() + 1); //0 day is Sunday
        postString = postString.concat(makeDateToString(tempDate));
    } else if (rangeState === 3) {
        tempDate.setDate(1);
        postString = postString.concat(makeDateToString(tempDate));
    } else if (rangeState === 4) {
        tempDate.setMonth(0);
        tempDate.setDate(1);
        postString = postString.concat(makeDateToString(tempDate));
    } else {
        postString = postString.concat(makeDateToString(roomObject._date));
    }
//    console.log(tempDate);
    postString = postString.concat("&children=");
    postString = postString.concat(roomObject._child);
    postString = postString.concat("&adults=");
    postString = postString.concat(roomObject._adult);
    postString = postString.concat("&range=");
    postString = postString.concat(roomObject._range);
    postString = postString.concat("&time=")
    postString = postString.concat(roomObject._time);
    postString = postString.concat("&check=");
    postString = postString.concat(checkForOverlap);

//    console.log(postString);

    return postString;
}

//Makes a new dataset for the date points to be highlighted.
function makeHighlightPoint(roomObject, graphList) {
    var label = (roomObject._roomID);
    label = label.concat(" - ");
    label = label.concat(makeDateToString(roomObject._date));

    //TODO Make this better and expandable for more data points
    var highlightData = [ , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , , ];

    var posToAddData;
    if (rangeState === 2) {
        posToAddData = roomObject._date.getDay();
        highlightData[posToAddData] = graphList[posToAddData];
    } else if (rangeState === 3) {
        posToAddData = (roomObject._date.getDate() - 1);   //Date is 1 - 31  :(
        highlightData[posToAddData] = graphList[posToAddData];
    } else if (rangeState === 4) {
        posToAddData = roomObject._date.getMonth();
        highlightData[posToAddData] = graphList[posToAddData];
    }

    var highlightPoint = {
        label: label,
        data: highlightData,
        fill: false,
        spanGaps: false,
        pointRadius: 7,
        pointBackgroundColor: "rgba(255, 255, 255, 1)",
        borderColor: "rgba(0, 0, 0, 0)",
        backgroundColor: "rgba(255, 255, 255, 1)"
    }

    return highlightPoint;
}

//Requests data from server and draws data on graph
function addRoomToGraph(roomObject) {
    //Get line data for a specific room
    var xhttpGraph = new XMLHttpRequest();
    xhttpGraph.open("POST", "/chart/getChartData", true);
    xhttpGraph.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    xhttpGraph.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var graphList = JSON.parse(xhttpGraph.response);

            console.log(graphList);

            if (graphList[0] == checkForOverlap) {      //Used to stop delay duplicate error

                graphList.splice(0, 1);

                //Check if data has been removed from the graph
                var removedFlag = false;
                for (o = 0; o < listOfRemoved.length; o++ ) {
                    if (listOfRemoved[o] === roomObject._objectID) {
                        removedFlag = true;
                    }
                }

                var blankDataset = {
                    label: ("remove"),
                    data: [],
                    fill: false,
                };

                //If data has been removed then don't draw it
                if (removedFlag === true) {
                    newDataset = blankDataset;
                    highlightPoint = blankDataset;
                } else {
                    //Makes new graph label
                    var label = (roomObject._roomID);
                    label = label.concat(" - ");
                    label = label.concat(makeDateToString(roomObject._date));
                    label = label.concat("    ");

                    //Makes highlighted point on the graph
                    var highlightPoint = makeHighlightPoint(roomObject, graphList);
                    highlightPoint.pointBackgroundColor = colours[roomObject.colour];
                    highlightPoint.backgroundColor = colours[roomObject.colour];


                    //Makes new dataset for graph
                    var newDataset = {
                        label: (label),
                        data: graphList,
                        fill: false,
                        borderColor: colours[roomObject.colour]
                    };

                    //Get biggest number and scale graph
                    var biggestValue = 0;
                    for (i = 0; i < graphList.length; i ++) {
                        if (graphList[i] > biggestValue) {
                            biggestValue = graphList[i];
                        }
                    }

                    //Sets largest number to 2 more than the biggest value
                    if (biggestValue > config.options.scales.yAxes[0].ticks.max) {
                        config.options.scales.yAxes[0].ticks.max = biggestValue + 1;
                    }
                }


                //Adds dataset to graph and updates graph
                config.data.datasets.push(newDataset);
                config.data.datasets.push(highlightPoint);
                window.myLine.update();
            }


        }
    };

    var postData = makePOSTData(roomObject);
    xhttpGraph.send(postData);

};
