#!/bin/bash
cd sass/
pwd
sass houseStyle.scss houseStyle.css
cd header/
pwd

sass header.scss header.css
cd ..
pwd
cd chartArea/
pwd

sass chartArea.scss chartArea.css
cd ..
pwd
cd footer/
pwd

sass footer.scss footer.css
cd ..
pwd
cd pages/
pwd

cd chart/
pwd
sass chart.scss chart.css
cd ..
pwd

cd help/
pwd
sass help.scss help.css
cd ..
pwd

cd settings/
pwd
sass settings.scss settings.css
cd ..
pwd

cd ..
cd settingsArea/
pwd
sass settingsArea.scss settingsArea.css
cd ..
pwd

cd helpArea/
pwd
sass helpArea.scss helpArea.css
cd ..
pwd