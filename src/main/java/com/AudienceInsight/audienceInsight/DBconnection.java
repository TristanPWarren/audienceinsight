package com.AudienceInsight.audienceInsight;

import oracle.jdbc.pool.OracleDataSource;
import java.sql.SQLException;
import java.util.Properties;
import java.io.InputStream;
import java.sql.Connection;

public class DBconnection {

    private static Connection connection = null;
    private static DBconnection instance = null;

    private static String driver;
    private static String ip;
    private static String port;
    private static String name;
    private static String user;
    private static String password;

    static {
        Properties prop = new Properties();
        try
        {
            InputStream inputStream = DBconnection.class.getClassLoader().
                    getResourceAsStream("config.properties");
            prop.load(inputStream);

            driver   = prop.getProperty("driver");
            ip       = prop.getProperty("ip");
            port     = prop.getProperty("port");
            name     = prop.getProperty("name");
            user     = prop.getProperty("user");
            password = prop.getProperty("password");
        }
        catch(Exception e)
        {
            e.printStackTrace();
            System.exit(1);
        }
    }

    public static DBconnection getInstance() {
        if (connection == null) {
            instance = new DBconnection();
        }
        return instance;
    }

    public Connection newConnection() {
        Connection connection = null;
        try
        {
            OracleDataSource ods = new OracleDataSource();
            ods.setURL(driver + ip + ":" + port + ":" + name);
            ods.setUser(user);
            ods.setPassword(password);
            connection = ods.getConnection();
            System.out.println(user + ": Connection to Oracle Database has been established.");
        }
        catch(SQLException sqle)
        {
            sqle.printStackTrace();
            System.exit(1);
        }

        return connection;
    }

}

