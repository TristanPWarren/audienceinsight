package com.AudienceInsight.audienceInsight;

public class AtomicEvent {
    private String time;
    private long adults;
    private long children;

    public AtomicEvent(String time, long adults, long children) {
        this.time     = time;
        this.adults   = adults;
        this.children = children;
    }

    public String getTime() {
        return time;
    }

    public long getAdults() {
        return adults;
    }

    public long getChildren() {
        return children;
    }

    @Override
    public String toString() {
        return "AtomicEvent{" +
                "time='" + time + '\'' +
                ", adults=" + adults +
                ", children=" + children +
                '}';
    }
}
