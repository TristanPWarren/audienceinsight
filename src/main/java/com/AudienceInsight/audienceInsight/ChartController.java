package com.AudienceInsight.audienceInsight;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

@Controller
public class ChartController {

    @RequestMapping(value="/chart/getChartData", method = RequestMethod.POST)
    @ResponseBody
    public long[] returnChartData(@RequestParam(value = "startDate") String  date,
                                  @RequestParam(value = "time")      int     time,
                                  @RequestParam(value = "range")     long    range,
                                  @RequestParam(value = "roomName")  String  roomName,
                                  @RequestParam(value = "children")  boolean children,
                                  @RequestParam(value = "adults")    boolean adults,
                                  @RequestParam(value = "check")     int check)
    {
        // Start date in form        yyyy-MM-dd
        // time (hour of the day)    Int 0 to 24
        // IMPORTANT:
        // if range = 0 (hours) then time matters, otherwise don't use it

        if(range == 0)
        { // need 6+1 values - 10minutes in 1hour
            long[] valuesList = getHourlyData(date, time, roomName, children, adults, check);
            System.out.println("<Get Hourly Chart Data> request made.\n");
            return  valuesList;
        }
        else if(range == 1)
        { // need 24+1 values - hours in 1day
            long[] valuesList = getDailyData(date, roomName, children, adults, check);
            System.out.println("<Get Daily Chart Data> request made.\n");
            return  valuesList;
        }
        else if(range == 2)
        { // need 7 values - days in 1week
            long[] valuesList = getWeeklyData(date, roomName, children, adults, check);
            System.out.println("<Get Weekly Chart Data> request made.\n");

            return  valuesList;
        }
        else if(range == 3)
        { // need 28-31 values - days in 1month
            long[] valuesList = getMonthlyData(date, roomName, children, adults, check);
            System.out.println("<Get Monthly Chart Data> request made.\n");

            return  valuesList;
        }
        else if(range == 4)
        { // need 12 values - months in 1year
            long[] valuesList = getYearlyData(date, roomName, children, adults, check);
            System.out.println("<Get Yearly Chart Data> request made.\n");

            return  valuesList;
        }
        else
        { // ?? unknown range value
            long[] unknown = new long[1];
            unknown[0] = -1;
            return  unknown;
        }
    }

    @RequestMapping(value = "/values/test")
    @ResponseBody
    public long[] formatTest()
    {
        long[] valuesList = getHourlyData("2018-03-13", 20, "Hall", true, true, 1);
        return valuesList;
    }

    public long[] getHourlyData(String  day,
                                int     time,
                                String  roomName,
                                boolean children,
                                boolean adults,
                                int check)
    {
        long[] valuesList = new long[8];
        valuesList[0] = check;

        DateTimeFormatter dayFormat     = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTimeFormatter dayTimeFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

        DateTime dateTime  = dayFormat.parseDateTime(day);
        DateTime startDate = new DateTime(day).withHourOfDay(time);
        DateTime endDate   = new DateTime(day).withHourOfDay(time+1);

        String strDateTime  = dayTimeFormat.print(dateTime);
        String strStartDate = dayTimeFormat.print(startDate);
        String strEndDate   = dayTimeFormat.print(endDate);

        System.out.println("Date: "      + dateTime);
        System.out.println("Hour start: " + startDate);
        System.out.println("Hour end:   " + endDate);

        System.out.println("Str Date: "      + strDateTime);
        System.out.println("Str Hour start: " + strStartDate);
        System.out.println("Str Hour   end: " + strEndDate);

        List<AtomicEvent> eventsListIN  = DBaccess.getEventsIN( strStartDate, strEndDate, roomName);
        List<AtomicEvent> eventsListOUT = DBaccess.getEventsOUT(strStartDate, strEndDate, roomName);
        long[] valuesIN  = new long[6];
        long[] valuesOUT = new long[6];

        for(AtomicEvent event : eventsListIN) {
            System.out.println("Event In: " + event);
            DateTime eventDateTime = dayTimeFormat.parseDateTime(event.getTime());
            int minute = eventDateTime.getMinuteOfHour();
            System.out.println("Event minute: " + minute);
            int range  = minute / 10;
            System.out.println("Event range: "  + range);
            if(adults == true)
                valuesIN[range] += event.getAdults();
            if(children == true)
                valuesIN[range] += event.getChildren();
        }
        for(AtomicEvent event : eventsListOUT) {
            System.out.println("Event Out: " + event);
            DateTime eventDateTime = dayTimeFormat.parseDateTime(event.getTime());
            int minute = eventDateTime.getMinuteOfHour();
            System.out.println("Event minute: " + minute);
            int range  = minute / 10;
            System.out.println("Event range: "  + range);
            if(adults == true)
                valuesOUT[range] += event.getAdults();
            if(children == true)
                valuesOUT[range] += event.getChildren();
        }

        long[] sumsIN    = new long[6];
        long[] sumsOUT   = new long[6];
        sumsIN[0]  = valuesIN[0];
        sumsOUT[0] = valuesOUT[0];
        valuesList[1] = sumsIN[0];

        for(int i = 1; i < 6; i++)
        {
            sumsIN[i] = sumsIN[i-1] + valuesIN[i];
            sumsOUT[i] = sumsOUT[i-1] + valuesOUT[i];

            valuesList[i+1] = sumsIN[i] - sumsOUT[i-1];
        }

        startDate = endDate;
        endDate   = endDate.plusMinutes(10);

        strStartDate = dayTimeFormat.print(startDate);
        strEndDate   = dayTimeFormat.print(endDate);

        System.out.println("Next Hour: "         + strStartDate);
        System.out.println("Next Hour + 10min: " + strEndDate);

        List<AtomicEvent> last10minEvents  = DBaccess.getEventsIN( strStartDate, strEndDate, roomName);
        for(AtomicEvent event : last10minEvents) {
            if(adults == true)
                valuesList[7] += event.getAdults();
            if(children == true)
                valuesList[7] += event.getChildren();
        }

        return valuesList;
    }

    public long [] getDailyData(String  day,
                                String  roomName,
                                boolean children,
                                boolean adults,
                                int check)
    {
        long[] valuesList = new long[26];
        valuesList[0] = check;

        DateTimeFormatter dayFormat     = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTimeFormatter dayTimeFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

        DateTime dateTime  = dayFormat.parseDateTime(day);
        DateTime startDate = new DateTime(day).hourOfDay().withMinimumValue().withTimeAtStartOfDay();
        DateTime endDate   = new DateTime(day).plusDays(1).hourOfDay().withMinimumValue().withTimeAtStartOfDay();

        String strDateTime  = dayTimeFormat.print(dateTime);
        String strStartDate = dayTimeFormat.print(startDate);
        String strEndDate   = dayTimeFormat.print(endDate);

        System.out.println("Date: "      + dateTime);
        System.out.println("First hour: " + startDate);
        System.out.println("Last  hour: " + endDate);

        System.out.println("Str Date: "      + strDateTime);
        System.out.println("Str First hour: " + strStartDate);
        System.out.println("Str Last  hour: " + strEndDate);

        List<AtomicEvent> eventsListIN  = DBaccess.getEventsIN( strStartDate, strEndDate, roomName);
        List<AtomicEvent> eventsListOUT = DBaccess.getEventsOUT(strStartDate, strEndDate, roomName);
        long[] valuesIN  = new long[24];
        long[] valuesOUT = new long[24];

        for(AtomicEvent event : eventsListIN) {
            System.out.println("Event In: " + event);
            DateTime eventDateTime = dayTimeFormat.parseDateTime(event.getTime());
            int hour = eventDateTime.getHourOfDay();
            if(adults == true)
                valuesIN[hour] += event.getAdults();
            if(children == true)
                valuesIN[hour] += event.getChildren();
        }
        for(AtomicEvent event : eventsListOUT) {
            System.out.println("Event Out: " + event);
            DateTime eventDateTime = dayTimeFormat.parseDateTime(event.getTime());
            int hour = eventDateTime.getHourOfDay();
            if(adults == true)
                valuesOUT[hour] += event.getAdults();
            if(children == true)
                valuesOUT[hour] += event.getChildren();
        }

        long[] sumsIN    = new long[24];
        long[] sumsOUT   = new long[24];
        sumsIN[0]  = valuesIN[0];
        sumsOUT[0] = valuesOUT[0];
        valuesList[1] = sumsIN[0];

        for(int i = 1; i < 24; i++)
        {
            sumsIN[i] = sumsIN[i-1] + valuesIN[i];
            sumsOUT[i] = sumsOUT[i-1] + valuesOUT[i];

            valuesList[i+1] = sumsIN[i] - sumsOUT[i-1];
        }

        startDate = endDate;
        endDate   = endDate.plusHours(1);

        strStartDate = dayTimeFormat.print(startDate);
        strEndDate   = dayTimeFormat.print(endDate);

        System.out.println("Next Day First hour: " + strStartDate);
        System.out.println("Next Day Last  hour: " + strEndDate);

        List<AtomicEvent> lastHourEvents  = DBaccess.getEventsIN( strStartDate, strEndDate, roomName);
        for(AtomicEvent event : lastHourEvents) {
            if(adults == true)
                valuesList[25] += event.getAdults();
            if(children == true)
                valuesList[25] += event.getChildren();
        }

        return valuesList;
    }

    public long[] getWeeklyData(String  week,
                                String  roomName,
                                boolean children,
                                boolean adults,
                                int check)
    {
        long[] valuesList = new long[8];
        valuesList[0] = check;

        DateTimeFormatter dayFormat     = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTimeFormatter dayTimeFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

        DateTime dateTime  = dayFormat.parseDateTime(week);
        DateTime startDate = new DateTime(week).dayOfWeek().withMinimumValue().withTimeAtStartOfDay();
        DateTime endDate   = new DateTime(week).plusWeeks(1).dayOfWeek().withMinimumValue().withTimeAtStartOfDay();

        String strDateTime  = dayTimeFormat.print(dateTime);
        String strStartDate = dayTimeFormat.print(startDate);
        String strEndDate   = dayTimeFormat.print(endDate);

        System.out.println("Date: "      + dateTime);
        System.out.println("First day: " + startDate);
        System.out.println("Last  day: " + endDate);

        System.out.println("Str Date: "      + strDateTime);
        System.out.println("Str First day: " + strStartDate);
        System.out.println("Str Last  day: " + strEndDate);

        List<AtomicEvent> eventsList = DBaccess.getEventsIN(strStartDate, strEndDate, roomName);
        for(AtomicEvent event : eventsList) {
            System.out.println(event);
            DateTime eventDateTime = dayTimeFormat.parseDateTime(event.getTime());
            int day = eventDateTime.getDayOfWeek();
            if(adults == true)
                valuesList[day] += event.getAdults();
            if(children == true)
                valuesList[day] += event.getChildren();
        }

        return valuesList;
    }

    public long[] getMonthlyData(String  month,
                                 String  roomName,
                                 boolean children,
                                 boolean adults,
                                 int check)
    {
        long[] valuesList = new long[32];
        valuesList[0] = check;

        DateTimeFormatter dayFormat     = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTimeFormatter dayTimeFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

        DateTime dateTime  = dayFormat.parseDateTime(month);
        DateTime startDate = new DateTime(month).dayOfMonth().withMinimumValue().withTimeAtStartOfDay();
        DateTime endDate   = new DateTime(month).plusMonths(1).dayOfMonth().withMinimumValue().withTimeAtStartOfDay();

        String strDateTime  = dayTimeFormat.print(dateTime);
        String strStartDate = dayTimeFormat.print(startDate);
        String strEndDate   = dayTimeFormat.print(endDate);

        System.out.println("Date: "      + dateTime);
        System.out.println("First day: " + startDate);
        System.out.println("Last  day: " + endDate);

        System.out.println("Str Date: "      + strDateTime);
        System.out.println("Str First day: " + strStartDate);
        System.out.println("Str Last  day: " + strEndDate);

        List<AtomicEvent> eventsList = DBaccess.getEventsIN(strStartDate, strEndDate, roomName);
        for(AtomicEvent event : eventsList) {
            System.out.println(event);
            DateTime eventDateTime = dayTimeFormat.parseDateTime(event.getTime());
            int day = eventDateTime.getDayOfMonth();
            if(adults == true)
                valuesList[day] += event.getAdults();
            if(children == true)
                valuesList[day] += event.getChildren();
        }

        return valuesList;
    }

    public long[] getYearlyData(String  year,
                                String  roomName,
                                boolean children,
                                boolean adults,
                                int check)
    {
        long[] valuesList = new long[13];
        valuesList[0] = check;

        DateTimeFormatter dayFormat     = DateTimeFormat.forPattern("yyyy-MM-dd");
        DateTimeFormatter dayTimeFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");

        DateTime dateTime  = dayFormat.parseDateTime(year);
        DateTime startDate = new DateTime(year).dayOfYear().withMinimumValue().withTimeAtStartOfDay();
        DateTime endDate   = new DateTime(year).plusYears(1).dayOfYear().withMinimumValue().withTimeAtStartOfDay();

        String strDateTime  = dayTimeFormat.print(dateTime);
        String strStartDate = dayTimeFormat.print(startDate);
        String strEndDate   = dayTimeFormat.print(endDate);

        System.out.println("Date: "      + dateTime);
        System.out.println("First day: " + startDate);
        System.out.println("Last  day: " + endDate);

        System.out.println("Str Date: "      + strDateTime);
        System.out.println("Str First day: " + strStartDate);
        System.out.println("Str Last  day: " + strEndDate);

        List<AtomicEvent> eventsList = DBaccess.getEventsIN(strStartDate, strEndDate, roomName);

        for(AtomicEvent event : eventsList) {
            System.out.println(event);
            DateTime eventDateTime = dayTimeFormat.parseDateTime(event.getTime());

            int month = eventDateTime.getMonthOfYear();
            if(adults == true)
                valuesList[month] += event.getAdults();
            if(children == true)
                valuesList[month] += event.getChildren();
        }

        return valuesList;
    }
}
