package com.AudienceInsight.audienceInsight;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AudienceInsightApplication {

	public static void main(String[] args) {
		SpringApplication.run(AudienceInsightApplication.class, args);
	}
}
