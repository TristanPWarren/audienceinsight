package com.AudienceInsight.audienceInsight;

import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.text.SimpleDateFormat;

public class DBaccess {

    // get new connection
    private static Connection conn = DBconnection.getInstance().newConnection();

    private static Timestamp StringToTimestamp(String text) {
        try {
            // specify timestamp format here
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            Date parsedDate = format.parse(text);
            Timestamp ts = new java.sql.Timestamp(parsedDate.getTime());

            return ts;
        }
        catch (Exception e) {
            System.out.println("Exception :" + e);
            return null;
        }
    }

    // Query for getting a kinect by ID
    public static long getKinectId(String kinectName)
    {
        long result = 0;
        String sqlQueryString =
                "SELECT ID " +
                "FROM KINECT " +
                "WHERE NAME = ?";

        try (PreparedStatement stmt = conn.prepareStatement(sqlQueryString)) {
            stmt.setString(1, kinectName);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                result = rs.getLong("ID");
            }
        }
        catch (SQLException e) {
            System.out.println("GET KINECT, SQL Query Error: " + e.getMessage());
        }
        catch (Exception e) {
            System.out.println("GET KINECT, Query Error: " + e.getMessage());
        }
        return result;
    }

    // Query for getting a room by ID
    public static long getRoomId(String roomName)
    {
        long result = 0;
        String sqlQueryString =
                "SELECT ID " +
                "FROM ROOM WHERE " +
                "NAME = ?";

        try (PreparedStatement stmt = conn.prepareStatement(sqlQueryString)) {
            stmt.setString(1, roomName);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                result = rs.getLong("ID");
            }
        }
        catch (SQLException e) {
            System.out.println("GET ROOM_ID, SQL Query Error: " + e.getMessage());
        }
        catch (Exception e) {
            System.out.println("GET ROOM_ID, Query Error: " + e.getMessage());
        }
        return result;
    }

    // Query template for getting ALL visitor events for people entering a room
    public static List<AtomicEvent> getAllEventsIN(String roomName)
    {
        List<AtomicEvent> resultList = new ArrayList<>();

        String insertTableSQL =
                "SELECT VISITOR.KINECT_ID, IN_OUT, TIME, NUM_ADULTS, NUM_CHILDREN FROM VISITOR " +
                        "LEFT JOIN ROOM_IN ON ROOM_IN.KINECT_ID = VISITOR.KINECT_ID " +
                        "LEFT JOIN ROOM RTI ON RTI.ID = ROOM_IN.ROOM_ID " +
                        "LEFT JOIN ROOM_OUT ON ROOM_OUT.KINECT_ID = VISITOR.KINECT_ID " +
                        "LEFT JOIN ROOM RTO ON RTO.ID = ROOM_OUT.ROOM_ID " +
                        "WHERE ((RTI.NAME = ? AND RTI.ID = ROOM_IN.ROOM_ID AND VISITOR.IN_OUT = 1) " +
                        "OR (RTO.NAME = ? AND RTO.ID = ROOM_OUT.ROOM_ID AND VISITOR.IN_OUT = 0)) ";

        try (PreparedStatement preparedStatement = conn.prepareStatement(insertTableSQL)) {
            preparedStatement.setString(1, roomName);
            preparedStatement.setString(2, roomName);

            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                resultList.add(
                        new AtomicEvent( rs.getString( "TIME"),
                                         rs.getLong(   "NUM_ADULTS"),
                                         rs.getLong(   "NUM_CHILDREN")));
            }
        }
        catch (SQLException e)
        {
            System.out.println("SQL <getEventsIN> Error: " + e.getMessage());
        }
        catch (Exception e)
        {
            System.out.println("<getEventsIN> Error: " + e.getMessage());
        }
        return resultList;
    }

    // Query template for getting filtered visitor events for people entering a room
    public static List<AtomicEvent> getEventsIN(String startDate, String endDate, String roomName)
    {
        List<AtomicEvent> resultList = new ArrayList<>();

        String insertTableSQL =
                "SELECT VISITOR.KINECT_ID, IN_OUT, TIME, NUM_ADULTS, NUM_CHILDREN FROM VISITOR " +
                "LEFT JOIN ROOM_IN ON ROOM_IN.KINECT_ID = VISITOR.KINECT_ID " +
                "LEFT JOIN ROOM RTI ON RTI.ID = ROOM_IN.ROOM_ID " +
                "LEFT JOIN ROOM_OUT ON ROOM_OUT.KINECT_ID = VISITOR.KINECT_ID " +
                "LEFT JOIN ROOM RTO ON RTO.ID = ROOM_OUT.ROOM_ID " +
                "WHERE VISITOR.TIME > ? AND VISITOR.TIME < ? AND " +
                "((RTI.NAME = ? AND RTI.ID = ROOM_IN.ROOM_ID AND VISITOR.IN_OUT = 1) " +
                "OR (RTO.NAME = ? AND RTO.ID = ROOM_OUT.ROOM_ID AND VISITOR.IN_OUT = 0)) ";

        Timestamp start = StringToTimestamp(startDate);
        Timestamp end   = StringToTimestamp(endDate);

        try (PreparedStatement preparedStatement = conn.prepareStatement(insertTableSQL)) {
            preparedStatement.setTimestamp(1, start);
            preparedStatement.setTimestamp(2, end);
            preparedStatement.setString(   4, roomName);
            preparedStatement.setString(   3, roomName);

            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                resultList.add(
                        new AtomicEvent( rs.getString( "TIME"),
                                         rs.getLong(   "NUM_ADULTS"),
                                         rs.getLong(   "NUM_CHILDREN")));
            }
        }
        catch (SQLException e)
        {
            System.out.println("SQL <getEventsIN> Error: " + e.getMessage());
        }
        catch (Exception e)
        {
            System.out.println("<getEventsIN> Error: " + e.getMessage());
        }
        return resultList;
    }

    // Query template for getting filtered visitor events for people leaving a room
    public static List<AtomicEvent> getEventsOUT(String startDate, String endDate, String roomName)
    {
        List<AtomicEvent> resultList = new ArrayList<>();

        String insertTableSQL =
                "SELECT VISITOR.KINECT_ID, IN_OUT, TIME, NUM_ADULTS, NUM_CHILDREN FROM VISITOR " +
                "LEFT JOIN ROOM_IN ON ROOM_IN.KINECT_ID = VISITOR.KINECT_ID " +
                "LEFT JOIN ROOM RTI ON RTI.ID = ROOM_IN.ROOM_ID " +
                "LEFT JOIN ROOM_OUT ON ROOM_OUT.KINECT_ID = VISITOR.KINECT_ID " +
                "LEFT JOIN ROOM RTO ON RTO.ID = ROOM_OUT.ROOM_ID " +
                "WHERE VISITOR.TIME > ? AND VISITOR.TIME < ? AND " +
                "((RTI.NAME = ? AND RTI.ID = ROOM_IN.ROOM_ID AND VISITOR.IN_OUT = 0) " +
                "OR (RTO.NAME = ? AND RTO.ID = ROOM_OUT.ROOM_ID AND VISITOR.IN_OUT = 1)) ";

        Timestamp start = StringToTimestamp(startDate);
        Timestamp end   = StringToTimestamp(endDate);

        try (PreparedStatement preparedStatement = conn.prepareStatement(insertTableSQL)) {
            preparedStatement.setTimestamp(1, start);
            preparedStatement.setTimestamp(2, end);
            preparedStatement.setString(   3, roomName);
            preparedStatement.setString(   4, roomName);

            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                resultList.add(
                        new AtomicEvent(rs.getString( "TIME"),
                                         rs.getLong(   "NUM_ADULTS"),
                                         rs.getLong(   "NUM_CHILDREN")));
            }
        }
        catch (SQLException e)
        {
            System.out.println("SQL <getEventsOUT> Error: " + e.getMessage());
        }
        catch (Exception e)
        {
            System.out.println("<getEventsOUT> Error: " + e.getMessage());
        }
        return resultList;
    }

    // Query template for getting a list of Visitor events
    public static List<VisitorEvent> getVisitorsQuery(String sqlQueryString)
    {
        List<VisitorEvent> resultList = new ArrayList<>();
        try (PreparedStatement stmt = conn.prepareStatement(sqlQueryString)) {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                resultList.add(
                        new VisitorEvent(rs.getLong(   "KINECT_ID"),
                                         rs.getBoolean("IN_OUT"),
                                         rs.getString( "TIME"),
                                         rs.getLong(   "NUM_ADULTS"),
                                         rs.getLong(   "NUM_CHILDREN")));
            }
        }
        catch (SQLException e) {
            System.out.println("SQL Query Error: " + e.getMessage());
        }
        catch (Exception e) {
            System.out.println("Query Error: " + e.getMessage());
        }
        return resultList;
    }

    // get all visitor events
    public static List<VisitorEvent> getVisitorData() {
        String queryString = "SELECT * FROM VISITOR";
        List<VisitorEvent> resultList = DBaccess.getVisitorsQuery(queryString);
        return resultList;
    }

    // Query template for getting a list of all rooms / kinects
    public static List<String> getNamesQuery(String sqlQueryString)
    {
        List<String> resultList = new ArrayList<>();

        try (PreparedStatement stmt = conn.prepareStatement(sqlQueryString)) {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                resultList.add(rs.getString("NAME"));
            }
        }
        catch (SQLException e) {
            System.out.println("SQL Query Error: " + e.getMessage());
        }
        catch (Exception e) {
            System.out.println("Query Error: " + e.getMessage());
        }
        return resultList;
    }

    // get all visitor events
    public static String[] getAllRooms() {
        String queryString = "SELECT NAME FROM ROOM";
        String[] resultList = DBaccess.getNamesQuery(queryString).toArray(new String[0]);
        return resultList;
    }

    // get all visitor events
    public static String[] getAllKinects() {
        String queryString = "SELECT NAME FROM KINECT";
        String[] resultList = DBaccess.getNamesQuery(queryString).toArray(new String[0]);
        return resultList;
    }

    // Query template for getting kinect info
    public static List<KinectDTO> getKinectsInfoQuery(String sqlQueryString)
    {
        List<KinectDTO> resultList = new ArrayList<>();

        try (PreparedStatement stmt = conn.prepareStatement(sqlQueryString)) {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                resultList.add(
                        new KinectDTO(rs.getString("KinectName"),
                                      rs.getString("RoomIn"),
                                      rs.getString("RoomOut")));
            }
        }
        catch (SQLException e) {
            System.out.println("SQL Query Error: " + e.getMessage());
        }
        catch (Exception e) {
            System.out.println("Query Error: " + e.getMessage());
        }
        return resultList;
    }

    // Get a table with all the kinects and thier corresponding RoomIn and RoomOut
    public static KinectDTO[] getKinectsInfo() {
        String queryString =
                "SELECT KINECT.NAME AS KinectName, " +
                "RIN.NAME AS RoomIn, " +
                "ROUT.NAME AS RoomOut " +
                "FROM KINECT " +
                "LEFT JOIN ROOM_IN ON ROOM_IN.KINECT_ID = KINECT.ID " +
                "LEFT JOIN ROOM_OUT ON ROOM_OUT.KINECT_ID = KINECT.ID " +
                "LEFT JOIN ROOM RIN ON RIN.ID = ROOM_IN.ROOM_ID " +
                "LEFT JOIN ROOM ROUT ON ROUT.ID = ROOM_OUT.ROOM_ID ";

        KinectDTO[] resultList = DBaccess.getKinectsInfoQuery(queryString).toArray(new KinectDTO[0]);
        return resultList;
    }

    // add new visitor event
    public static boolean addVisitorEvent(VisitorEvent event) {
        String insertTableSQL =
                "INSERT INTO VISITOR " +
                "(KINECT_ID, IN_OUT, TIME, NUM_ADULTS, NUM_CHILDREN) " +
                "VALUES(?,?,?,?,?)";

        Timestamp ts = StringToTimestamp(event.getTime());
        try (PreparedStatement preparedStatement = conn.prepareStatement(insertTableSQL)) {
            preparedStatement.setLong(     1, event.getKinectId());
            preparedStatement.setBoolean(  2, event.getInOut());
            preparedStatement.setTimestamp(3, ts);
            preparedStatement.setLong(     4, event.getAdults());
            preparedStatement.setLong(     5, event.getChildren());

            preparedStatement.executeUpdate();
            return true;
        }
        catch (SQLException e) {
            System.out.println("SQL <AddVisitorEvent> Error: " + e.getMessage());
            return false;
        }
        catch (Exception e) {
            System.out.println("<AddVisitorEvent> Error: " + e.getMessage());
            return false;
        }
    }

    // add a new room
    public static boolean addRoom(String room) {
        String insertTableSQL =
                "INSERT INTO ROOM " +
                "(NAME) " +
                "VALUES(?)";

        try (PreparedStatement preparedStatement = conn.prepareStatement(insertTableSQL)) {
            preparedStatement.setString(1, room);

            preparedStatement.executeUpdate();
            return true;
        }
        catch (SQLException e) {
            System.out.println("SQL <AddRoom> Error: " + e.getMessage());
            return false;
        }
        catch (Exception e) {
            System.out.println("<AddRoom> Error: " + e.getMessage());
            return false;
        }
    }

    public static boolean addKinect(String kinect, String mac) {
        String insertTableSQL =
                 "INSERT INTO KINECT " +
                 "(NAME, MAC) " +
                 "VALUES(?,?)";

        try (PreparedStatement preparedStatement = conn.prepareStatement(insertTableSQL)) {
            preparedStatement.setString(1, kinect);
            preparedStatement.setString(2, mac);

            preparedStatement.executeUpdate();
            return true;
        }
        catch (SQLException e) {
            System.out.println("SQL <AddKinect> Error: " + e.getMessage());
            return false;
        }
        catch (Exception e) {
            System.out.println("<AddKinect> Error: " + e.getMessage());
            return false;
        }
    }

    public static boolean addKinectToRoomIn(String kinect, String roomIn) {
        long kinectId = getKinectId(kinect);
        long roomId = getRoomId(roomIn);

        String insertTableSQL =
                "INSERT INTO ROOM_IN " +
                "(KINECT_ID, ROOM_ID) " +
                "VALUES(?,?)";

        try (PreparedStatement preparedStatement = conn.prepareStatement(insertTableSQL)) {
            preparedStatement.setLong(1, kinectId);
            preparedStatement.setLong(2, roomId);

            preparedStatement.executeUpdate();
            return true;
        }
        catch (SQLException e) {
            System.out.println("SQL <AddKinectToRoomIn> Error: " + e.getMessage());
            return false;
        }
        catch (Exception e) {
            System.out.println("<AddKinectToRoomIn> Error: " + e.getMessage());
            return false;
        }
    }

    public static boolean addKinectToRoomOut(String kinect, String roomOut) {
        if (roomOut == null)
            return true;
        else {
            long kinectId = getKinectId(kinect);
            long roomId = getRoomId(roomOut);

            String insertTableSQL =
                    "INSERT INTO ROOM_OUT " +
                    "(KINECT_ID, ROOM_ID) " +
                    "VALUES(?,?)";
            System.out.println(insertTableSQL);

            try (PreparedStatement preparedStatement = conn.prepareStatement(insertTableSQL)) {
                preparedStatement.setLong(1, kinectId);
                preparedStatement.setLong(2, roomId);

                preparedStatement.executeUpdate();
                return true;
            } catch (SQLException e) {
                System.out.println("SQL <AddKinectToRoomOut> Error: " + e.getMessage());
                return false;
            } catch (Exception e) {
                System.out.println("<AddKinectToRoomOut> Error: " + e.getMessage());
                return false;
            }
        }
    }

    public static boolean updateRoomName(String oldName, String newName) {
        String updateTableSQL =
                "UPDATE ROOM " +
                "SET NAME = ? " +
                "WHERE NAME = ?";
        try (PreparedStatement preparedStatement = conn.prepareStatement(updateTableSQL))
        {
            preparedStatement.setString(2, oldName);
            preparedStatement.setString(1, newName);

            preparedStatement.executeUpdate();
            return true;
        }
        catch (SQLException e)
        {
            System.out.println("SQL <UpdateRoomName> Error: " + e.getMessage());
            return false;
        }
        catch (Exception e)
        {
            System.out.println("<UpdateRoomName> Error: " + e.getMessage());
            return false;
        }
    }

    public static boolean updateKinectName(String oldName, String newName) {
        String updateTableSQL =
                "UPDATE KINECT " +
                "SET NAME = ? " +
                "WHERE NAME = ?";
        try (PreparedStatement preparedStatement = conn.prepareStatement(updateTableSQL))
        {
            preparedStatement.setString(1, newName);
            preparedStatement.setString(2, oldName);

            preparedStatement.executeUpdate();
            return true;
        }
        catch (SQLException e)
        {
            System.out.println("SQL <UpdateKinectName> Error: " + e.getMessage());
            return false;
        }
        catch (Exception e)
        {
            System.out.println("<UpdateKinectName> Error: " + e.getMessage());
            return false;
        }
    }

    public static boolean deleteRoom(String roomName) {
        String deleteRowSQL =
                "DELETE FROM ROOM " +
                "WHERE NAME = ?";

        try (PreparedStatement preparedStatement = conn.prepareStatement(deleteRowSQL)) {
            preparedStatement.setString(1, roomName);
            preparedStatement.executeUpdate();
            return true;
        }
        catch (SQLException e) {
            System.out.println("SQL <DeleteRoom> Error: " + e.getMessage());
            return false;
        }
        catch (Exception e) {
            System.out.println("<DeleteRoom> Error: " + e.getMessage());
            return false;
        }

    }

    public static String getStartDate() {
        String queryString =
                "SELECT MIN(TIME) AS StartDate FROM VISITOR";

        String result = "";
        try (PreparedStatement stmt = conn.prepareStatement(queryString)) {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                result = rs.getString("StartDate");
            }
        }
        catch (SQLException e) {
            System.out.println("SQL Query Error: " + e.getMessage());
        }
        catch (Exception e) {
            System.out.println("Query Error: " + e.getMessage());
        }
        return result;
    }
    public static String getEndDate() {
        String queryString =
                "SELECT MAX(TIME) AS EndDate FROM VISITOR";

        String result = "";
        try (PreparedStatement stmt = conn.prepareStatement(queryString)) {
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                result = rs.getString("EndDate");
            }
        }
        catch (SQLException e) {
            System.out.println("SQL Query Error: " + e.getMessage());
        }
        catch (Exception e) {
            System.out.println("Query Error: " + e.getMessage());
        }
        return result;
    }

}
