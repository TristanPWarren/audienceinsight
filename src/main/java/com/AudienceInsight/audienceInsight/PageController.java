package com.AudienceInsight.audienceInsight;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.*;

@Controller
public class PageController {

    @RequestMapping("/settings")
    public String room1(Model model) {
        return "settings";
    }

    @RequestMapping("/help")
    public String help(Model model) {
        return "help";
    }

    @RequestMapping("/chart")
    public String chart(Model model) {
        System.out.println("Page change");
        return "chart";
    }

}
