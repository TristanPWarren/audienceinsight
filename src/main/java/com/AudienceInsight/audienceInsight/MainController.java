package com.AudienceInsight.audienceInsight;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@Controller
public class MainController {

    @RequestMapping(value = "/get/Data")
    @ResponseBody
    public VisitorEvent[] getAllEntries() {
        List rList = DBaccess.getVisitorData();
        System.out.println("<Get All Entries> request made.\n");
        return (VisitorEvent[]) rList.toArray(new VisitorEvent[rList.size()]);
    }

    @RequestMapping(value = "/get/Rooms")
    @ResponseBody
    public List getAllRooms() {
        List rList = Arrays.asList(DBaccess.getAllRooms());
        System.out.println("<Get All Rooms> request made.\n");
        return rList;
    }

    @RequestMapping(value = "/get/Kinects")
    @ResponseBody
    public List getAllKinects() {
        List rList = Arrays.asList(DBaccess.getAllKinects());
        System.out.println("<Get All Kinects> request made.\n");
        return rList;
    }

    @RequestMapping(value = "/get/Kinects/info")
    @ResponseBody
    public List getKinectsInfo() {
        List rList = Arrays.asList(DBaccess.getKinectsInfo());
        System.out.println("<Get Kinects Info> request made.\n");
        return rList;
    }

    @PostMapping("/add/VisitorEvent")
    @ResponseBody
    public ResponseEntity addVisitorEvent(@RequestParam(value = "kinectId") long    kinectId,
                                          @RequestParam(value = "inOut")    boolean inOut,
                                          @RequestParam(value = "time")     String  time,
                                          @RequestParam(value = "adults")   long    adults,
                                          @RequestParam(value = "children") long    children)
    {
        VisitorEvent event =
                new VisitorEvent(kinectId, inOut, time, adults, children);

        if (DBaccess.addVisitorEvent(event))
        {
            System.out.println("<Add Visitor Event> request made.\n");
            return new ResponseEntity<>(null, HttpStatus.CREATED);
        }
        else {
            System.out.println("<Add Visitor Event> request failed.v");
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/add/Room")
    @ResponseBody
    public ResponseEntity addRoom(@RequestParam(value = "roomName") String room)
    {
        if (DBaccess.addRoom(room))
        {
            System.out.println("<Add Room> request made.\n");
            return new ResponseEntity<>(null, HttpStatus.CREATED);
        }
        else {
            System.out.println("<Add Room> request failed.\n");
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/add/Kinect/in")
    @ResponseBody
    public ResponseEntity addKinectIn(@RequestParam(value = "kinect") String kinect,
                                      @RequestParam(value = "roomIn") String roomIn)
    {
        if (DBaccess.addKinectToRoomIn(kinect, roomIn))
        {
            System.out.println("<Add Kinect In> request made.\n");
            return new ResponseEntity<>(null, HttpStatus.CREATED);
        }
        else {
            System.out.println("<Add Kinect In> request failed.\n");
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/add/Kinect/out")
    @ResponseBody
    public ResponseEntity addKinectOut(@RequestParam(value = "kinect")  String kinect,
                                       @RequestParam(value = "roomOut") String roomOut)
    {
        if (DBaccess.addKinectToRoomOut(kinect, roomOut))
        {
            System.out.println("<Add Kinect Out> request made.\n");
            return new ResponseEntity<>(null, HttpStatus.CREATED);
        }
        else {
            System.out.println("<Add Kinect Out> request failed.\n");
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "update/Room")
    @ResponseBody
    public ResponseEntity updateRoom(@RequestParam(value = "oldName") String oldName,
                                     @RequestParam(value = "newName") String newName)
    {
        if (DBaccess.updateRoomName(oldName, newName))
        {
            System.out.println("<Update Room> request made.\n");
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
        else {
            System.out.println("<Update Room> request failed.\n");
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(value = "update/Kinect")
    @ResponseBody
    public ResponseEntity updateKinect(@RequestParam(value = "oldName") String oldName,
                                       @RequestParam(value = "newName") String newName)
    {
        if (DBaccess.updateKinectName(oldName, newName))
        {
            System.out.println("<Update Kinect> request made.\n");
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
        else {
            System.out.println("<Update Kinect> request failed.\n");
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping("/delete/Room")
    @ResponseBody
    public ResponseEntity deleteRoom(@RequestParam(value = "name") String roomName)
    {
        if (DBaccess.deleteRoom(roomName))
        {
            System.out.println("<Delete Room> request made.\n");
            return new ResponseEntity<>(null, HttpStatus.OK);
        }
        else {
            System.out.println("<Delete Room> request failed.\n");
            return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
        }
    }
}
