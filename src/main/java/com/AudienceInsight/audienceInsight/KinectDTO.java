package com.AudienceInsight.audienceInsight;

public class KinectDTO {
    private String name;
    private String roomIn;
    private String roomOut;

    public KinectDTO(String name, String roomIn, String roomOut) {

        this.name = name;
        this.roomIn = roomIn;
        this.roomOut = roomOut;
    }

    public String getName() {
        return name;
    }

    public String getRoomIn() {
        return roomIn;
    }

    public String getRoomOut() {
        return roomOut;
    }
}
