package com.AudienceInsight.audienceInsight;

public class VisitorEvent {
    private long kinectId;
    private boolean inOut;
    private String time;
    private long adults;
    private long children;

    public VisitorEvent(long kinectId, boolean inOut, String time, long adults, long children) {
        this.kinectId = kinectId;
        this.inOut    = inOut;
        this.time     = time;
        this.adults   = adults;
        this.children = children;
    }

    public long getKinectId() {
        return kinectId;
    }

    public boolean getInOut() {
        return inOut;
    }

    public String getTime() {
        return time;
    }

    public long getAdults() {
        return adults;
    }

    public long getChildren() {
        return children;
    }

    public void setKinectId(long kinectId) {
        this.kinectId = kinectId;
    }

    public void setInOut(boolean inOut) {
        this.inOut = inOut;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setAdults(long adults) {
        this.adults = adults;
    }

    public void setChildren(long children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "VisitorEvent{" +
                "kinectId=" + kinectId +
                ", inOut=" + inOut +
                ", time='" + time + '\'' +
                ", adults=" + adults +
                ", children=" + children +
                '}';
    }
}
