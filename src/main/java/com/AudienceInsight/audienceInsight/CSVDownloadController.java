package com.AudienceInsight.audienceInsight;

import javax.servlet.http.HttpServletResponse;

import org.joda.time.LocalDate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.supercsv.io.ICsvListWriter;
import org.supercsv.io.CsvListWriter;
import org.supercsv.prefs.CsvPreference;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

@Controller
public class CSVDownloadController {
    @RequestMapping(value = "/download/Csv")
    public void downloadCSV(HttpServletResponse response) throws IOException
    {
        String csvFileName = "records.csv";

        response.setContentType("text/csv");

        String headerKey = "Content-Disposition";
        String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);
        response.setHeader(headerKey, headerValue);

        // uses the Super CSV API to generate CSV data from the model data
        ICsvListWriter csvWriter = new CsvListWriter(response.getWriter(), CsvPreference.STANDARD_PREFERENCE);

        // get a list of all rooms
        String queryString = "SELECT NAME FROM ROOM";
        List<String> roomList = DBaccess.getNamesQuery(queryString);

        // setup DateTime formatters
        DateTimeFormatter dayTimeFormat = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
        DateTimeFormatter dayFormat     = DateTimeFormat.forPattern("yyyy-MM-dd");

        LocalDate startDate = LocalDate.parse(DBaccess.getStartDate(), dayTimeFormat);
        LocalDate endDate   = LocalDate.parse(DBaccess.getEndDate()  , dayTimeFormat);

        System.out.println("Begin day: " + startDate);
        System.out.println("End   day: " + endDate);

        // construct an event list for each room
        HashMap<String, List<AtomicEvent>> allEventsList = new HashMap<>();
        for(String room : roomList) {
            List<AtomicEvent> roomEvent = DBaccess.getAllEventsIN(room);
            allEventsList.put(room, roomEvent);
        }

        // construct the CSV data structure skeleton
        HashMap<LocalDate, HashMap<String, Integer>> csvTable = new HashMap<>();
        for (LocalDate day = startDate; day.isBefore(endDate.plusDays(1)); day = day.plusDays(1))
        {
            HashMap<String, Integer> row = new HashMap<>();
            for(String room : roomList) {
                row.put(room, 0);
            }
            csvTable.put(day, row);
        }

        // calculate CSV data
        for(String room : allEventsList.keySet())
        {
            List<AtomicEvent> eventListByRoom = allEventsList.get(room);
            for(AtomicEvent event : eventListByRoom)
            {
                LocalDate dayOfEvent   = dayTimeFormat.parseLocalDate(event.getTime());
                Integer   oldValue     = csvTable.get(dayOfEvent).get(room);
                Integer   updatedValue = oldValue + (int) (long) (event.getAdults() + event.getChildren());
                csvTable.get(dayOfEvent).put(room, updatedValue);
            }
        }

        // construct the CSV file
        List<String> header = new ArrayList<>();
        header.add("DATE");
        header.addAll(roomList);

        csvWriter.writeHeader(header.toArray(new String[0]));

        for (LocalDate day = startDate; day.isBefore(endDate.plusDays(1)); day = day.plusDays(1))
        {
            List<String> fileRow = new ArrayList<>();
            fileRow.add(dayFormat.print(day));
            for(String room : roomList) {
                fileRow.add(String.valueOf(csvTable.get(day).get(room)));
            }

            csvWriter.write(fileRow);
        }

        System.out.println("<Download CSV file> request made.\n");
        csvWriter.close();
    }
}
